//Darina Hojeij, 2246076
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args ){
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("A", 21, 40);
        bicycles[1] = new Bicycle("B", 27, 50);
        bicycles[2] = new Bicycle("C", 19, 30);
        bicycles[3] = new Bicycle("D", 30, 60);

        for (Bicycle x : bicycles){
            System.out.println(x);
        }
    }
}
