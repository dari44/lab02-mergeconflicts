//Darina Hojeij, 2246076
package vehicles;

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    
    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", Maxspeed: " + this.maxSpeed;
    }
}